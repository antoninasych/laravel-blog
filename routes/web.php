<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::resource('posts', 'FrontController', ['only' => ['show', 'index']]);
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'cabinet', 'middleware' => ['auth']], function () {
    Route::match(['get'], '/pages', 'PageController@anyData')->name('list');
    Route::match(['post'], '/upload', 'PageController@upload');
    Route::resource('page', 'PageController');
});


