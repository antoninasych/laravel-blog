<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('/css/main.css') }}">


</head>
<body>


<div class="row">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="container">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1 class="title">{{$post->title()->value()}} </h1>
                    </div>
                    <img src="/upload/{{$post->picture}}" class="img-responsive">
                    <hr class="">
                    <div class="col-md-12">
                    <?php     $trimmed = trim($post->content, '"');
                    echo $trimmed;?>
                    </div>
                    <div class="pull-right" style="margin-top: 35px">
                        <hr>
                    <div>{{$post->created_at}}</div>
                    <div>{{$post->authorRelation->name}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>