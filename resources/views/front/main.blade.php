<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('/css/main.css') }}">


</head>
<body>

<div class="content">
    <div class="col-md-12" style="margin: 0 auto">
        @if($articles)
            <div class="panel panel-default">
                <div class="jumbotron">
                    <h1>Blog</h1>
                    <p>For young and energy peope</p>
                </div>
                <div class="row">
                    <div class="center-block">
                        @foreach($articles as $article)
                            <div class="col-md-5" style="max-width: 500px">
                                <div class="thumbnail article-list" style="max-width: 550px">
                                    <div class="view view-first"
                                    ">
                                    <img src={{ asset("/upload/".$article->picture)}}  class="img-responsive">
                                    <div class="mask">
                                        <h2 style="margin-top: 10%;color: #fff ">{{  Str::limit($article->title,50) }}</h2>
                                        <p>{{  $article->created_at }}</p>

                                        <a href="{{route('posts.show',['id'=>$article->id])}}" class="info">
                                            Подробнее </a>
                                    </div>

                                </div>
                                <div class="caption"><a
                                            href="{{route('posts.show',['id'=>$article->id])}}">{{  Str::limit($article->title,50) }}</a>
                                </div>
                            </div>
                    </div>
                    @endforeach
                </div>
                <div class="content">
                    <div class="col-md-12">
                        <ul class="pagination">
                            @if($articles->lastPage() > 1)
                                @if($articles->currentPage() !== 1)
                                    <li class="active"></li>
                                    <li><a href="{{ $articles->url(($articles->currentPage() - 1)) }}">&laquo;</a></li>

                                @endif
                                @for($i = 1; $i <= $articles->lastPage(); $i++)
                                    @if($articles->currentPage() == $i)
                                        <li><a class="selected disabled">{{ $i }}</a></li>
                                    @else
                                        <li><a href="{{ $articles->url($i) }}">{{ $i }}</a></li>
                                    @endif
                                @endfor
                                @if($articles->currentPage() !== $articles->lastPage())
                                    <li><a href="{{ $articles->url(($articles->currentPage() + 1)) }}">&raquo;</a></li>
                                @endif
                            @endif
                        </ul>
                        @endif
                    </div>
                </div>

            </div>
    </div>
</div>
</div>
</body>
</html>