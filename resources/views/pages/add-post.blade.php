@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Добавить новую статью</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Добавление статьи</h3>
        </div>
        <div class="box-body">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif

            <form method="POST"
                  action="{{(isset($post->id))?route('page.update',['id' =>$post->id] ):route('page.store')}}"
                  enctype="multipart/form-data">

                @if(isset($post->id))  <input type="hidden" name="id" value="{{$post->id}}">
                {{ method_field('PUT') }}
                @endif

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Заголовок</label>
                            <input type="text" name="title" placeholder="Заголовок статьи." class="form-control"
                                   value="{{(isset($post))?$post->title()->value():''}}"
                            >
                            @if($errors->has('title'))
                                <p class="text-red">Заголовок статьи некорректный</p>
                            @endif

                        </div>

                        <div class="col-md-12">
                            <label>Содержимое статьи</label>
                            <textarea name="content" id="content" class="form-control ckeditor"
                                      placeholder="Содержимое статьи.">{{(isset($post))?$post->content()->value():''}}</textarea>
                            @if($errors->has('content'))
                                <p class="text-red">Содержимое статьи некорректно</p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                            @if(isset($post))
                                <?php echo \App\Classes\Helpers\StringHelper::getImageDecor($post->picture()->value(), 50) ?>
                            @endif
                        </div>
                        <div class="col-md-11">
                            <label>Изображение на главной</label><input type="file" name="image" class="form-control">
                            @if($errors->has('image'))
                                <p class="text-red">Недопустимое изображение. Формат должен быть из списка:
                                    peg,png,jpg,gif,svg,
                                    не более 2048 кб</p>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="pull-left col-md-12">
                            <input type="submit" class="btn-primary btn form-control" value="Отправить">
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </div>
    <script>
        CKEDITOR.replace('content', {
            disallowedContent: 'a[href];',
            height: 300,
            filebrowserUploadUrl: '/cabinet/upload'
        });
    </script>
@stop

<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>