@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Список статей</h1>
@stop


@section('content')
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('success') !!}</li>
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Список статей</h3>
                    <div class="box-body" >
                        <table class="table table-hover" id="posts-table">
                            <thead>
                            <tr role="row">
                                <th> ID</th>
                                <th> Картинка</th>
                                <th> Заголовок</th>
                                <th> Содержание</th>
                                <th> Автор</th>
                                <th> Просмотр</th>
                                <th> Удаление</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
<script src="{{ asset('/js/libraries/jquery.js') }}"></script>
<script src="{{ asset('js/libraries/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('js/libraries/datatables/dataTables.bootstrap.min.js') }}"></script>

<link rel="stylesheet" href="{{ asset('/css/libraries/datatables/dataTables.bootstrap.min.css') }}">

<script>
    (function ($) {
        $(function () {


            $('#posts-table').DataTable({
                processing: true,
                "language": {
                    "url": "/Russian.json"
                },
                serverSide: true,
                ajax: '{!! route('list') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'picture', name: 'picture',},
                    {data: 'title', name: 'title'},
                    {data: 'content', name: 'content'},
                    {data: 'author', name: 'author'},
                    {data: 'view_details', name: 'view_details', searchable: false},
                    {data: 'remove', name: 'remove', searchable: false}
                ]
            });




        });
    })(jQuery);

</script>
