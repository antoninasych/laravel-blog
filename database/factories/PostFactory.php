<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Http\Models\Posts::class, function (Faker $faker) {
    return [
        'title' => $faker->text(255),
        'content' => $faker->text(1000),
        'picture' => '1555873375.jpg',
        'created_at' => \Carbon\Carbon::now()
    ];
});
