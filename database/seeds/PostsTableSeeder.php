<?php

use Illuminate\Database\Seeder;


class PostsTableSeeder extends Seeder
{
    public $quontity = 50;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create()->each(function ($user) {
            $user->posts()->save(factory(App\Http\Models\Posts::class)->make());
        });
    }
}
