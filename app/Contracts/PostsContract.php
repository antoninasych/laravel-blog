<?php

namespace App\Contracts;


use App\Http\Models\Posts;
use App\Values\Posts\PostsId;


interface PostsContract
{
    public function getList($select, $take);

    public function getOne(PostsId $postId): Posts;

    public function save(Posts $post): void;

    public function delete(Posts $posts): void;

}