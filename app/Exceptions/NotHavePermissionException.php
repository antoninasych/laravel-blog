<?php


namespace App\Exceptions;


class NotHavePermissionException extends BaseException
{
    public $message = "Недостаточно прав";
    public $code = 500;

}