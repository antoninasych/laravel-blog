<?php

namespace App\Classes\Helpers;

class StringHelper
{
    public static function cutStr($str, $lenght = 100, $end = ' …', $charset = 'UTF-8', $token = '~')
    {
        $str = strip_tags($str);
        if (mb_strlen($str, $charset) >= $lenght) {
            $wrap = wordwrap($str, $lenght, $token);
            $str_cut = mb_substr($wrap, 0, mb_strpos($wrap, $token, 0, $charset), $charset);
            return $str_cut .= $end;
        } else {
            return $str;
        }
    }

    public static function getImageDecor($picture, $height=100, $width=100)
    {
        return '<img src="' . '/upload/' . $picture. '" style="max-height:'.$height.'px">';

    }
}