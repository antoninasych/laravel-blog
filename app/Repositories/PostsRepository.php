<?php


namespace App\Repositories;


use App\Contracts\PostsContract;
use App\Http\Models\Posts;
use App\Values\Posts\PostsId;


class PostsRepository implements PostsContract
{
    private $posts;

    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }

    public function getOne(PostsId $postId): Posts
    {
        $post = $this->posts->findOrFail($postId->value());

        return $post;

    }

    public function save(Posts $post): void
    {
        $post->saveOrFail();
    }

    public function delete(Posts $posts): void
    {
        $posts->delete();
    }

    public function getList($select = "*", $take = FALSE)
    {
        $builder = $this->posts->select($select);
        if ($take) {
            $builder->take($take);
            return ($builder->paginate($take));
        }

        return $builder->get();
    }
}