<?php


namespace App\Values\Posts;



final class PostsTitle
{
    private $rawTitle;

    private function __construct(string $rawTitle)
    {
        $this->validateTitle($rawTitle);
        $this->rawTitle = $rawTitle;
    }

    public static function new(string $rawTitle): PostsTitle
    {
        return new self($rawTitle);
    }

    public function value(): string
    {
        return $this->rawTitle;
    }

    public function __toString(): string
    {
        return (string)$this->rawTitle;
    }

    public function validateTitle(string $rawTitle): void
    {
       //make additional validation if needs
    }

}