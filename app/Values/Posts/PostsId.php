<?php


namespace App\Values\Posts;


final class PostsId
{
    private $rawId;

    private function __construct(int $rawId)
    {
        $this->rawId = $rawId;
    }

    public static function new(int $rawId): PostsId
    {
        return new self($rawId);
    }

    public function value(): int
    {
        return $this->rawId;
    }

    public function isEqualTo(PostsId $roleId): bool
    {
        return $this->value() === $roleId->value();
    }
}