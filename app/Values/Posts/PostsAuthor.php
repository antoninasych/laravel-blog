<?php


namespace App\Values\Posts;


use App\User;

final class PostsAuthor
{
    private $rawUser;

    private function __construct(int $rawUser)
    {
        $this->validateTitle($rawUser);
        $this->rawUser = $rawUser;
    }

    public static function new(int $rawUser): PostsAuthor
    {
        return new self($rawUser);
    }

    public function value(): string
    {
        return $this->rawUser;
    }

    public function __toString(): string
    {
        return (string)$this->rawUser;
    }

    public function validateTitle(string $rawUser): void
    {
        //make additional validation if needs
    }
    public function isEqualTo(PostsAuthor $author): bool
    {

        return  $this->value() === $author->value();
    }
}