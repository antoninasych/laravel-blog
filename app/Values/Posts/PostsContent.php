<?php


namespace App\Values\Posts;


final class PostsContent
{
    private $rawContent;

    private function __construct(string $rawContent)
    {
        $this->validateTitle($rawContent);
        $this->rawContent = $rawContent;
    }

    public static function new(string $rawContent): PostsContent
    {
        return new self($rawContent);
    }

    public function value(): string
    {
        return $this->rawContent;
    }

    public function __toString(): string
    {
        return (string)$this->rawContent;
    }

    public function validateTitle(string $rawContent): void
    {
        //make additional validation if needs
    }
}