<?php


namespace App\Values\Posts;



final class PostsImage
{
    private $rawImage;

    private function __construct(string $rawImage)
    {
        $this->validateImage($rawImage);
        $this->rawImage = $rawImage;
    }

    public static function new(string $rawImage): PostsImage
    {
        return new self($rawImage);
    }

    public function value(): string
    {
        return $this->rawImage;
    }

    public function __toString(): string
    {
        return (string)$this->rawImage;
    }

    public function validateImage(string $rawImage): void
    {
       //make additional validation if needs
    }

}