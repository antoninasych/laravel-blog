<?php


namespace App\Http\Controllers\Api;


use App\Classes\Helpers\ApiResponse;
use App\Contracts\PostsContract;
use App\Values\Posts\PostsId;
use Illuminate\Http\Request;


class ApiPostsController
{
    public $posts;
    public $request;

    public function __construct(PostsContract $postsContract, Request $request)
    {
        $this->posts = $postsContract;
        $this->request = $request;
    }


    public function index()
    {
        try {
            $take = ($this->request->has('take')) ? $take = $this->request->get('take') : false;
            $select = ($this->request->has('select')) ? $this->request->get('select') : '*';

            $posts = $this->posts->getList($select, $take);
        } catch (\Throwable $exception) {
            return ApiResponse::badResponse('Невозможно отобразить данные');
        }
        return ApiResponse::goodResponse($posts);
    }

    public function show(int $id)
    {
        try {
            $posts = $this->posts->getOne(PostsId::new($id));
        } catch (\Throwable $exception) {
            return ApiResponse::badResponse('Данная статья не существует');
        }
        return ApiResponse::goodResponse($posts);
    }

}