<?php


namespace App\Http\Controllers;


use App\Contracts\PostsContract;
use App\Values\Posts\PostsId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontController
{
    private $posts;
    private $request;
    private $user;

    public function __construct(PostsContract $postsContract, Request $request)
    {
        $this->posts = $postsContract;
        $this->request = $request;
        $this->user = Auth::user();
    }

    public function index()
    {
        return view('front.main')->with(
            ['articles' => $this->posts->getList(
                "*",
                $take = 9
            )]);

    }

    public function show(int $id)
    {
        $id = PostsId::new($id);
        return view('front.detail')->with(['post' => $this->posts->getOne($id)]);
    }
}