<?php


namespace App\Http\Controllers;


use App\Classes\Helpers\ApiResponse;
use App\Classes\Helpers\StringHelper;
use App\Contracts\PostsContract;
use App\Exceptions\NotHavePermissionException;
use App\Http\Models\Posts;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Values\Posts\PostsAuthor;
use App\Values\Posts\PostsContent;
use App\Values\Posts\PostsId;
use App\Values\Posts\PostsImage;
use App\Values\Posts\PostsTitle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;


class PageController
{
    private $posts;
    private $request;
    private $user;

    public function __construct(PostsContract $postsContract, Request $request)
    {
        $this->posts = $postsContract;
        $this->request = $request;
        $this->user = Auth::user();
    }

    public function anyData()
    {
        $posts = $this->posts->getList($select = "*", $take = FALSE);
        return Datatables::of($posts)
            ->addColumn('id', function ($posts) {
                return $posts->id()->value();
            })
            ->editColumn('picture', function ($posts) {
                return StringHelper::getImageDecor($posts->picture()->value(), 50);
            })
            ->editColumn('title', function ($posts) {
                return $posts->title()->value();
            })
            ->editColumn('content', function ($posts) {
                return StringHelper::cutStr($posts->content()->value(), 200);
            })
            ->editColumn('author', function ($posts) {
                return $posts->authorRelation->name;
            })
            ->addColumn('view_details', function ($posts) {
                return '<a class="btn btn-default" href="' . route('page.edit', ['id' => $posts->id()->value()]) . '"><i class="fa fa-fw fa-edit"></i></a>';
            })
            ->addColumn('remove', function ($posts) {
                return '<a class="btn btn-danger" href="cabinet/pages/destroy/' . $posts->id()->value() . '"><i class="fa fa-fw fa-remove"></i></a>';
            })
            ->rawColumns(['view_details', 'picture', 'remove'])
            ->make(true);
    }

    public function index()
    {
        return view('pages.list-post');
    }

    public function create()
    {
        return view('pages.add-post');
    }

    public function edit(int $id)
    {
        $postId = PostsId::new(intval($id));
        $post = $this->posts->getOne($postId);

        return view('pages.add-post')->with(['post' => $post]);
    }


    public function store(StorePostRequest $request)
    {
        if ($request->validated()) {
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('/upload/'), $imageName);
            list($title, $content, $author) = $this->preparePost();
            $post = Posts::create($author, $title, $content);
            $image = PostsImage::new($imageName);
            $post->changeImage($image);
            $this->posts->save($post);
            return view('pages.add-post')->with(['post' => $post, 'success' => 'Статья была успешно добавлена']);

        }

        return redirect()->back();
    }

    private function preparePost()
    {
        $title = PostsTitle::new($this->request->get('title'));
        $content = PostsContent::new($this->request->get('content'));
        $author = PostsAuthor::new($this->user->getAuthIdentifier());
        return [
            $title,
            $content,
            $author
        ];
    }

    public function update(UpdatePostRequest $request, int $id)
    {
        $id = PostsId::new($id);
        $post = $this->posts->getOne($id);
        if ($request->validated()) {
            $title = PostsTitle::new($request->get('title'));
            $content = PostsContent::new($request->get('content'));
            $author = PostsAuthor::new($this->user->getAuthIdentifier());
            if (!$post->author()->isEqualTo($author)) {
                throw new NotHavePermissionException();
            }

            if (request()->image) {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('/upload/'), $imageName);
                $image = PostsImage::new($imageName);
                $post->changeImage($image);
            }

            $post->changeTitle($title);
            $post->changeContent($content);
            $post->save();
        }
        return view('pages.add-post')->with(['post' => $post, 'success' => 'Статья была успешно обновлена']);
    }

    public function destroy(int $id)
    {

        $id = PostsId::new($id);
        $post = $this->posts->getOne($id);
        $author = PostsAuthor::new($this->user->getAuthIdentifier());
        if (!$post->author()->isEqualTo($author)) {
            throw new NotHavePermissionException();
        }

        $this->posts->delete($post);

        return redirect()->back()->with('success', 'Статья была успешно удалена');
    }

    public function upload()
    {
        $errorMessage = 'Не удалось загрузить изображение';
        if ($this->request->has('upload')) {

            $file = $_FILES['upload']['tmp_name'];
            $file_name = $_FILES['upload']['name'];
            $file_name_array = explode(".", $file_name);
            $extension = end($file_name_array);
            $new_image_name = rand() . '.' . $extension;
            $allowed_extension = array("jpg", "gif", "png", "jpeg");
            $url = '';
            if (in_array($extension, $allowed_extension)) {
                move_uploaded_file($file, public_path('/upload/') . $new_image_name);
                $url = '/upload/' . $new_image_name;
                return ApiResponse::goodResponseSimple([
                    'uploaded' => 1,
                    'fileName' => $new_image_name,
                    "url" => $url
                ]);
            }
            $errorMessage = 'Допустимые форматы загрузки: "jpg", "gif", "png", "jpeg"' . $errorMessage;
        }

        return ApiResponse::goodResponseSimple([
            'uploaded' => 0,
            'error' => [
                'message' => $errorMessage
            ]
        ]);

    }


}