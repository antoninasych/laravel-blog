<?php


namespace App\Http\Models;


use App\User;
use App\Values\Posts\PostsAuthor;
use App\Values\Posts\PostsContent;
use App\Values\Posts\PostsId;
use App\Values\Posts\PostsImage;
use App\Values\Posts\PostsTitle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Posts extends Model
{
    protected $table = 'posts';

    /**
     * @param PostsAuthor $author
     * @param PostsTitle $title
     * @param PostsContent $content
     * @return Posts
     */
    public static function create(PostsAuthor $author, PostsTitle $title, PostsContent $content)
    {
        $document = new self();
        $document->changeAuthor($author);
        $document->changeTitle($title);
        $document->changeContent($content);

        return $document;
    }

    public function changeId(PostsId $id): void
    {
        $this->setAttribute('id', $id->value());
    }

    public function changeTitle(PostsTitle $title): void
    {
        $this->setAttribute('title', $title->value());
    }

    public function changeContent(PostsContent $content): void
    {
        $this->setAttribute('content', $content->value());
    }

    public function changeImage(PostsImage $image): void
    {
        $this->setAttribute('picture', $image->value());
    }

    public function changeAuthor(PostsAuthor $author): void
    {
        $this->setAttribute('author', $author->value());
    }

    public function id(): PostsId
    {
        return PostsId::new((string)$this->getAttribute('id'));
    }

    public function title(): PostsTitle
    {
        return PostsTitle::new((string)$this->getAttribute('title'));
    }

    public function content(): PostsContent
    {
        return PostsContent::new((string)$this->getAttribute('content'));
    }

    public function picture(): PostsImage
    {
        return PostsImage::new((string)$this->getAttribute('picture'));
    }


    public function author(): PostsAuthor
    {
        return PostsAuthor::new((int)$this->getAttribute('author'));
    }

    public function authorRelation()
    {
       return $this->hasOne(User::class, 'id','author');
    }
}